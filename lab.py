import argparse
from utils.utils import mkdir, save_flags
import os


def main(FLAGS):

    if FLAGS.algo == 'rnn':
        from rnn import main_cv, main_train, main_test
        
        if FLAGS.mode == 'cv':
            hyperparams = main_cv.main(FLAGS)
            
            with open(os.path.join(FLAGS.outroot, FLAGS.folder, FLAGS.drug, "hyperparams.txt"), 'w') as f:
                f.write('learning rate: %f\n'%hyperparams['lr'])
                f.write('batch size: %d\n'%hyperparams['bs'])
                f.write('num epoch: %d\n'%hyperparams['epoch'])

        else:
            # parameters are known
            # training with the specified hyperparams or hps found by CV
            # will ignore flags if mode != full
            hyperparams = {
                'lr': FLAGS.lr,
                'bs': FLAGS.batch_size,
                'epoch': FLAGS.max_epoch
            }

    if FLAGS.mode == 'test':
        main_test.main(FLAGS)
    else:
        model_list = main_train.main(FLAGS, hyperparams)

if __name__ == '__main__':

    parser = argparse.ArgumentParser()
    parser.add_argument("--dataroot", default="../TINDL/data/", help="root directory of the data")
    parser.add_argument("--folder", default="", help="directory of the output")
    parser.add_argument("--outroot", default="results/", help="root directory of the output")
    parser.add_argument("--algo", default="default", help="[default], rnn")
    parser.add_argument("--mode", default="cv", help="[full], custom, explain-only, no-explain, test")
    parser.add_argument("--drug", default='etoposide', help="drug name")
    parser.add_argument("--test_norm", default='tindl', help="[tindl], train, test")
    parser.add_argument("--seed", default=1, help="seed number for pseudo-random generation", type=int)
    parser.add_argument("--max_epoch", default=1000, help="maximum number of epoch", type=int)
    parser.add_argument("--lr", default=1e-4, help="learning rate (if mode is not full)", type=float)
    parser.add_argument("--batch_size", default=128, help="batch size (if mode is not full)", type=int)
    parser.add_argument("--ensemble", default=10, help="Number of models to ensemble", type=int)


    args = parser.parse_args()

    if args.mode != "test":
        mkdir(os.path.join(args.outroot, args.folder, args.drug))
        save_flags(args)
    main(args)



