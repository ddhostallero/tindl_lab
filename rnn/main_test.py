import numpy as np
import os
import time
from utils.data_initializer import initialize_train_and_test
from sklearn.preprocessing import StandardScaler
from rnn.trainer import Trainer
from utils.utils import setup_logger, mkdir
from torch.utils import data as th_data
import torch
import pandas as pd 


def test(FLAGS, tcga, genes, seed):
    """
    Calls the training method and tests afterwards (for one initialization)

    Input:
        FLAGS: arguments set by the user
        tcga: DataFrame that contains the normalizez test GEx as columns and sample names as index
        genes: list of genes to be used
        seed: seed for pseudo-random number generation
    Output:
        The trained model and its predictions
    """
    seed_dir = os.path.join(FLAGS.outroot, FLAGS.folder, FLAGS.drug, str(seed)) 
    model_dir = os.path.join(seed_dir, 'weights')

    trainer = Trainer(
        logger=None,
        learning_rate=None,
        n_genes=len(genes),
        model_dir=model_dir)

    # predict on the test set
    test_pred = trainer.predict_label(tcga.values)
    test_pred = test_pred.numpy()
    result = pd.DataFrame(test_pred, index=tcga.index, columns=[FLAGS.drug]).T    
    # result.to_csv(os.path.join(seed_dir, 'test_prediction_%s_norm.csv'%FLAGS.test_norm))
    result.to_csv(os.path.join(seed_dir, 'test_prediction.csv'))

    return trainer.model.cpu(), result

def main(FLAGS):
    """
    Trains the models for the final ensemble
    Input:
        FLAGS: arguments set by the user
    """

    mod_list = []
    orig_seed = int(FLAGS.seed)
    for i in range(1, FLAGS.ensemble+1):
        FLAGS.seed = i
        _, _, genes, tcga = initialize_train_and_test(FLAGS) # reseeding happens here
        mod, pred = test(FLAGS, tcga, genes, i)
        mod_list.append(mod)

        if i == 1:
            pred_df = pd.DataFrame(index=range(1, FLAGS.ensemble+1), columns=pred.columns)
        pred_df.loc[i] = pred.loc[FLAGS.drug]

    # save the ensemble predictions
    out = os.path.join(FLAGS.outroot, FLAGS.folder, FLAGS.drug, "ensemble_predictions_%s_norm.csv"%FLAGS.test_norm)
    pred_df.mean().to_csv(out)
    FLAGS.seed = orig_seed

    return mod_list