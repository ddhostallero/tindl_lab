import torch
import torch.nn as nn
import torch.nn.functional as F


class myRNN(nn.Module):
  def __init__(self, n_in):
    super(myRNN, self).__init__()

    self.lstm = nn.LSTM(input_size=1565, hidden_size=512)    
    self.l3 = nn.Linear(512, 128)

    self.drp = nn.Dropout(0.5)
    self.l4 = nn.Linear(128, 1)

  def forward(self, x):
    x = x.view(-1, 10, 1565)
    x,_ = self.lstm(x)
    x = F.relu(self.l3(x[:,-1,:]))
    x = self.drp(x)
    return self.l4(x)